package com.seryozha.hovhannisyan.testingdevice.helper;

import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceEntity;
import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceTypeEnum;
import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import com.seryozha.hovhannisyan.testingdevice.model.DeviceDTO;
import lombok.experimental.UtilityClass;

import java.time.OffsetDateTime;
import java.util.UUID;

@UtilityClass
public class DeviceHelper {

    public DeviceEntity generateDeviceEntity() {
        var entity = new DeviceEntity();
        entity.setId(System.currentTimeMillis());
        entity.setDeviceName(randomString());
        entity.setDeviceId(UUID.randomUUID().toString());
        entity.setDeviceType(DeviceTypeEnum.MOBILE);
        entity.setBooked(true);
        entity.setBookedBy(randomString());
        entity.setBookedOn(OffsetDateTime.now());

        return entity;
    }

    public DeviceDTO generateDeviceDto() {
        var dto = new DeviceDTO();
        dto.setName(randomString());
        dto.setAvailability("No");
        dto.setWhenBooked(OffsetDateTime.now());
        dto.setWhoBooked(randomString());
        return dto;
    }

    public BookDeviceRQDTO generateBookDeviceRQDTO() {
        return BookDeviceRQDTO.builder()
                .whoBooked(randomString())
                .build();
    }

    public String randomString() {
        return String.format("%d", System.currentTimeMillis());
    }
}
