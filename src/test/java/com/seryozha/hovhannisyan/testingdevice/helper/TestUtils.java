package com.seryozha.hovhannisyan.testingdevice.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import net.minidev.json.JSONObject;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

@UtilityClass
public class TestUtils {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String readString(String fileName) {
        try {
            InputStream stream = TestUtils.class.getClassLoader().getResourceAsStream(fileName);
            return IOUtils.toString(stream, "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file " + fileName, e);
        }
    }

    public static JSONObject readData(String data) {
        try {
            return OBJECT_MAPPER.readValue(data, JSONObject.class);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read data " + data, e);
        }
    }

    public static String toJson(Object data) {
        try {
            return OBJECT_MAPPER.writeValueAsString(data);
        } catch (IOException e) {
            throw new RuntimeException("Failed to write data " + data, e);
        }
    }
}
