package com.seryozha.hovhannisyan.testingdevice.api;

import com.seryozha.hovhannisyan.testingdevice.helper.DeviceHelper;
import com.seryozha.hovhannisyan.testingdevice.helper.TestUtils;
import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DeviceApiTest {

    @Autowired
    private MockMvc mockMvc;

    private Long incorrectId = 650L;
    private Long id = 5L;

    @DisplayName("GET / Get all devices")
    @Order(0)
    @Test
    void allDevices() throws Exception {
        var response = mockMvc.perform(get("/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        var expectedDataPath = "Devices/response.verify.json";
        var expected = TestUtils.readString(expectedDataPath);
        JSONAssert.assertEquals(expected, response, true);
    }

    @DisplayName("GET /{id} Get a device by its id:Not found")
    @Test
    void getByIdNotFound() throws Exception {
        mockMvc.perform(get(String.format("/%d", incorrectId))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode", is("404")))
                .andExpect(jsonPath("$.message", is(String.format("The requested device (%d) can not be found.", incorrectId))));
    }

    @DisplayName("GET /{id} Get a device by its id:5")
    @Test
    @Order(3)
    void getByIdSuccess() throws Exception {
        var response = mockMvc.perform(get(String.format("/%d", id))
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        var expectedDataPath = "Device/response.5.verify.json";
        var expected = TestUtils.readString(expectedDataPath);
        JSONAssert.assertEquals(expected, response, true);
    }

    @DisplayName("PUT /reserve-device/{id} Reserve a device by its id:Not found")
    @Test
    void reserveDeviceNotFound() throws Exception {
        var requestJson = TestUtils.toJson(DeviceHelper.generateBookDeviceRQDTO());
        mockMvc.perform(put(String.format("/reserve-device/%d", incorrectId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                )
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode", is("404")))
                .andExpect(jsonPath("$.message", is(String.format("The requested device (%d) can not be found.", incorrectId))));
    }

    @DisplayName("PUT /reserve-device/{id} Reserve a device by its id:Bad Request")
    @Test
    void reserveDeviceBadRequest() throws Exception {
        var requestJson = TestUtils.toJson(BookDeviceRQDTO.builder().build());
        mockMvc.perform(put(String.format("/reserve-device/%d", id))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode", is("400")))
                .andExpect(jsonPath("$.message", is("Please specify who books the device")));
    }

    @DisplayName("PUT /reserve-device/{id} Reserve a device by its id:5")
    @Test
    @Order(1)
    void reserveDeviceSuccess() throws Exception {
        var request = DeviceHelper.generateBookDeviceRQDTO();
        var requestJson = TestUtils.toJson(request);
        mockMvc.perform(put(String.format("/reserve-device/%d", id))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Apple iPhone 13")))
                .andExpect(jsonPath("$.availability", is("No")))
                .andExpect(jsonPath("$.whoBooked", is(request.getWhoBooked())));

    }

    @DisplayName("PUT /cancel-device-reservation/{id} Canceled the device reservation:Not found")
    @Test
    void cancelDeviceReservationNotFound() throws Exception {
        mockMvc.perform(put(String.format("/cancel-device-reservation/%d", incorrectId))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode", is("404")))
                .andExpect(jsonPath("$.message", is(String.format("The requested device (%d) can not be found.", incorrectId))));
    }

    @DisplayName("PUT /cancel-device-reservation/{id} Canceled the device reservation:5")
    @Test
    @Order(2)
    void cancelDeviceReservationSuccess() throws Exception {
        var response = mockMvc.perform(put(String.format("/cancel-device-reservation/%d", id))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andReturn()
                .getResponse()
                .getContentAsString();
        var expectedDataPath = "Device/response.5.verify.json";
        var expected = TestUtils.readString(expectedDataPath);
        JSONAssert.assertEquals(expected, response, true);
    }
}
