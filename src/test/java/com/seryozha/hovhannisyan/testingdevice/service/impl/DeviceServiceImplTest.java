package com.seryozha.hovhannisyan.testingdevice.service.impl;

import com.seryozha.hovhannisyan.testingdevice.dao.repository.DeviceRepository;
import com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException;
import com.seryozha.hovhannisyan.testingdevice.helper.DeviceHelper;
import com.seryozha.hovhannisyan.testingdevice.mapper.DeviceMapper;
import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DeviceServiceImplTest {

    @InjectMocks
    private DeviceServiceImpl service;
    @Mock
    private DeviceRepository repository;
    @Mock
    private DeviceMapper mapper;
    private InOrder inOrder;

    @BeforeEach
    public void init() {
        inOrder = inOrder(repository, mapper);
    }

    @Test
    void reserveDevice() {
        var entity = DeviceHelper.generateDeviceEntity();
        var dto = DeviceHelper.generateDeviceDto();
        Long id = System.currentTimeMillis();
        BookDeviceRQDTO rqdto = DeviceHelper.generateBookDeviceRQDTO();
        //mock
        when(repository.findById(id)).thenReturn(Optional.of(entity));
        when(repository.save(entity)).thenReturn(entity);
        when(mapper.convertToExternalObject(entity)).thenReturn(dto);
        //execute
        var response = service.reserveDevice(id, rqdto);
        //verify
        assertEquals(dto, response, "Should return expected mapped DTO");

        inOrder.verify(repository).findById(anyLong());
        inOrder.verify(repository).save(any());
        inOrder.verify(mapper).convertToExternalObject(any());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void reserveDeviceNotFound() {
        Long id = System.currentTimeMillis();
        BookDeviceRQDTO rqdto = DeviceHelper.generateBookDeviceRQDTO();
        //mock
        when(repository.findById(id)).thenReturn(Optional.empty());
        //execute
        assertThrows(DeviceNotFoundException.class, () -> service.reserveDevice(id, rqdto));
        //verify
        inOrder.verify(repository).findById(anyLong());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void cancelDeviceReservation() {
        var entity = DeviceHelper.generateDeviceEntity();
        var dto = DeviceHelper.generateDeviceDto();
        Long id = System.currentTimeMillis();
        //mock
        when(repository.findById(id)).thenReturn(Optional.of(entity));
        when(repository.save(entity)).thenReturn(entity);
        when(mapper.convertToExternalObject(entity)).thenReturn(dto);
        //execute
        var response = service.cancelDeviceReservation(id);
        //verify
        assertEquals(dto, response, "Should return expected mapped DTO");

        inOrder.verify(repository).findById(anyLong());
        inOrder.verify(repository).save(any());
        inOrder.verify(mapper).convertToExternalObject(any());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void cancelDeviceReservationNotFound() {

        Long id = System.currentTimeMillis();
        //mock
        when(repository.findById(id)).thenReturn(Optional.empty());
        //execute
        assertThrows(DeviceNotFoundException.class, () -> service.cancelDeviceReservation(id));
        //verify
        inOrder.verify(repository).findById(anyLong());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void getById() {
        var entity = DeviceHelper.generateDeviceEntity();
        var dto = DeviceHelper.generateDeviceDto();
        Long id = System.currentTimeMillis();
        //mock
        when(repository.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.convertToExternalObject(entity)).thenReturn(dto);
        //execute
        var response = service.getById(id);
        //verify
        assertEquals(dto, response, "Should return expected mapped DTO");

        inOrder.verify(repository).findById(anyLong());
        inOrder.verify(mapper).convertToExternalObject(any());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void getByIdNotFound() {
        Long id = System.currentTimeMillis();
        //mock
        when(repository.findById(id)).thenReturn(Optional.empty());
        //execute
        assertThrows(DeviceNotFoundException.class, () -> service.getById(id));
        //verify
        inOrder.verify(repository).findById(anyLong());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void getAll() {
        var entity = DeviceHelper.generateDeviceEntity();
        var dto = DeviceHelper.generateDeviceDto();
        //mock
        when(repository.findAll()).thenReturn(List.of(entity));
        when(mapper.convertToExternalObject(entity)).thenReturn(dto);
        //execute
        var response = service.getAll();
        //verify
        assertEquals(dto, response.get(0), "Should return expected mapped DTO");

        inOrder.verify(repository).findAll();
        inOrder.verify(mapper).convertToExternalObject(any());
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void getAllEmpty() {
        //mock
        when(repository.findAll()).thenReturn(List.of());
        //execute
        var response = service.getAll();
        //verify
        assertTrue(response.isEmpty(), "Should return empty list");

        inOrder.verify(repository).findAll();
        inOrder.verifyNoMoreInteractions();
    }
}