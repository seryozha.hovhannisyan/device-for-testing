package com.seryozha.hovhannisyan.testingdevice.mapper;

import com.seryozha.hovhannisyan.testingdevice.helper.DeviceHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DeviceMapperImpl.class})
class DeviceMapperTest {

    @Autowired
    private DeviceMapper mapper;

    @Test
    void convertToExternalObjectNull() {
        assertNull(mapper.convertToExternalObject(null), "Should return null when entity is null");
    }

    @Test
    void convertToExternalObject() {
        var entity = DeviceHelper.generateDeviceEntity();

        var dto = mapper.convertToExternalObject(entity);

        assertNotNull(dto, "Should return dto when entity is not null");
        assertEquals("No", dto.getAvailability(), "Device should not be available");
        assertEquals(entity.getDeviceName(), dto.getName(), "Should present device name");
        assertEquals(entity.getBookedBy(), dto.getWhoBooked(), "Should present who booked");
        assertEquals(entity.getBookedOn(), dto.getWhenBooked(), "Should present when booked");
    }

    @Test
    void bookedToAvailability() {
        assertEquals("No", DeviceMapper.bookedToAvailability(true), "Should be unavailable when booked");
        assertEquals("Yes", DeviceMapper.bookedToAvailability(false), "Should be available when no booked");
    }

}