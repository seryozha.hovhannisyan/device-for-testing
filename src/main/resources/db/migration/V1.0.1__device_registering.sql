/*Table Structure for table device*/
DROP TABLE IF EXISTS device;
CREATE TABLE device
(
    id          integer     NOT NULL,
    device_name varchar(64) NOT NULL,
    device_id   varchar(64) DEFAULT NULL,
    device_type varchar(64) NOT NULL,
    booked      BOOLEAN     DEFAULT false,
    booked_on   TIMESTAMP   DEFAULT NULL,
    booked_by   varchar(36) DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO device (id, device_name, device_type)
VALUES (1, 'Samsung Galaxy S9', 'MOBILE'),
       (2, '2x Samsung Galaxy S8', 'MOBILE'),
       (3, 'Motorola Nexus 6', 'MOBILE'),
       (4, 'Oneplus 9', 'MOBILE'),
       (5, 'Apple iPhone 13', 'MOBILE'),
       (6, 'Apple iPhone 12', 'MOBILE'),
       (7, 'Apple iPhone 11', 'MOBILE'),
       (8, 'iPhone X', 'MOBILE'),
       (9, 'Nokia 3310', 'MOBILE');
