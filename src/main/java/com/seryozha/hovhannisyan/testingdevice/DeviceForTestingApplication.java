package com.seryozha.hovhannisyan.testingdevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeviceForTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeviceForTestingApplication.class, args);
    }

}
