package com.seryozha.hovhannisyan.testingdevice.api;

import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import com.seryozha.hovhannisyan.testingdevice.model.DeviceDTO;
import com.seryozha.hovhannisyan.testingdevice.service.DeviceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping
@Validated
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @Operation(summary = "Reserve a device by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reserved the device",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = DeviceDTO.class))}),
            @ApiResponse(responseCode = "404", description = "device not found",
                    content = @Content)})
    @PutMapping("/reserve-device/{id}")
    public ResponseEntity<DeviceDTO> reserveDevice(@PathVariable(name = "id") Long id,
                                                   @Valid @RequestBody BookDeviceRQDTO rqdto) {
        return ResponseEntity.ok(deviceService.reserveDevice(id, rqdto));
    }

    @Operation(summary = "Cancel a device reservation by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Canceled the device reservation",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = DeviceDTO.class))}),
            @ApiResponse(responseCode = "404", description = "device not found",
                    content = @Content)})
    @PutMapping("/cancel-device-reservation/{id}")
    public ResponseEntity<DeviceDTO> cancelDeviceReservation(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(deviceService.cancelDeviceReservation(id));
    }

    @Operation(summary = "Get a device by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the device",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = DeviceDTO.class))}),
            @ApiResponse(responseCode = "404", description = "device not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<DeviceDTO> getById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(deviceService.getById(id));
    }

    @Operation(summary = "Get all devices")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Retrieve the devices",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = List.class))})
    })
    @GetMapping
    public ResponseEntity<List<DeviceDTO>> getAll() {
        return ResponseEntity.ok(deviceService.getAll());
    }
}
