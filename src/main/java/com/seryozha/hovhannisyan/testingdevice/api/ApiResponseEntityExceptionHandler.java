package com.seryozha.hovhannisyan.testingdevice.api;

import com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException;
import com.seryozha.hovhannisyan.testingdevice.model.ErrorDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ApiResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    public static final Locale LOCALE = Locale.US;

    private final MessageSource messageSource;

    @ExceptionHandler(DeviceNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleBaseException(DeviceNotFoundException ex) {
        var message = messageSource.getMessage(ex.getMessage(), new Object[]{ex.getId()}, LOCALE);
        var status = HttpStatus.NOT_FOUND;
        log.error(message);

        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(ErrorDTO.builder()
                        .message(message)
                        .errorCode(String.valueOf(status.value()))
                        .build());
    }

    @Override
    protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                          HttpHeaders headers,
                                                          HttpStatusCode status,
                                                          WebRequest request) {

        //Get all errors
        var messageOptional = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst();

        var message = messageOptional.isPresent() ? messageOptional.get() : "";

        var error = ErrorDTO.builder()
                .message(message)
                .errorCode(String.valueOf(status.value()))
                .build();
        return new ResponseEntity(error, headers, status);
    }
}
