package com.seryozha.hovhannisyan.testingdevice.dao.repository.converter;

import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceTypeEnum;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class DeviceTypeConverter implements AttributeConverter<DeviceTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(DeviceTypeEnum attribute) {
        return attribute != null ? attribute.name() : null;
    }

    @Override
    public DeviceTypeEnum convertToEntityAttribute(String dbData) {
        return DeviceTypeEnum.valueOf(dbData);
    }
}
