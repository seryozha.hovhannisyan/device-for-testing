package com.seryozha.hovhannisyan.testingdevice.dao.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.OffsetDateTime;

@Entity
@Table(name = "device")
@Data
public class DeviceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_name", nullable = false)
    private String deviceName;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "device_type", nullable = false)
    private DeviceTypeEnum deviceType;

    @Column(name = "booked")
    private boolean booked;

    @Column(name = "booked_on")
    private OffsetDateTime bookedOn;

    @Column(name = "booked_by")
    private String bookedBy;

}
