package com.seryozha.hovhannisyan.testingdevice.dao.repository;

import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<DeviceEntity, Long> {
}
