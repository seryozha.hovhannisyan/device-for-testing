package com.seryozha.hovhannisyan.testingdevice.model;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class DeviceDTO {
    private String name;
    private String availability;
    private OffsetDateTime whenBooked;
    private String whoBooked;
}
