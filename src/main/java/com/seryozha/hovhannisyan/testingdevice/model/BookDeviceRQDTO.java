package com.seryozha.hovhannisyan.testingdevice.model;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;


@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class BookDeviceRQDTO {
    @NotBlank(message = "{notBlank.whoBooked}")
    private String whoBooked;
}
