package com.seryozha.hovhannisyan.testingdevice.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ErrorDTO {
    private String message;
    private String errorCode;
}
