package com.seryozha.hovhannisyan.testingdevice.mapper;

import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceEntity;
import com.seryozha.hovhannisyan.testingdevice.model.DeviceDTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface DeviceMapper {

    @Named("bookedToAvailability")
    static String bookedToAvailability(boolean booked) {
        return booked ? "No" : "Yes";
    }

    @Mapping(source = "booked", target = "availability", qualifiedByName = "bookedToAvailability")
    @Mapping(source = "bookedOn", target = "whenBooked")
    @Mapping(source = "bookedBy", target = "whoBooked")
    @Mapping(source = "deviceName", target = "name")
    DeviceDTO convertToExternalObject(DeviceEntity internal);
}
