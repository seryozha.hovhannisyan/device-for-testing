package com.seryozha.hovhannisyan.testingdevice.exception;

import lombok.Getter;

@Getter
public class DeviceNotFoundException extends RuntimeException {

    private static final String messageKey = "exception.message.device.notfound";

    private final Long id;

    public DeviceNotFoundException(Long id) {
        super(messageKey);
        this.id = id;
    }
}
