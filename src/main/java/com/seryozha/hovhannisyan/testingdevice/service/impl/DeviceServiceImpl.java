package com.seryozha.hovhannisyan.testingdevice.service.impl;

import com.seryozha.hovhannisyan.testingdevice.dao.entity.DeviceEntity;
import com.seryozha.hovhannisyan.testingdevice.dao.repository.DeviceRepository;
import com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException;
import com.seryozha.hovhannisyan.testingdevice.mapper.DeviceMapper;
import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import com.seryozha.hovhannisyan.testingdevice.model.DeviceDTO;
import com.seryozha.hovhannisyan.testingdevice.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

@Slf4j
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService {
    private final DeviceRepository deviceRepository;
    private final DeviceMapper deviceMapper;

    @Override
    @Transactional
    public DeviceDTO reserveDevice(Long id, BookDeviceRQDTO rqdto) {
        var entity = getEntityById(id);
        entity.setBooked(true);
        entity.setBookedOn(OffsetDateTime.now());
        entity.setBookedBy(rqdto.getWhoBooked());
        entity = deviceRepository.save(entity);
        return deviceMapper.convertToExternalObject(entity);
    }

    @Override
    @Transactional
    public DeviceDTO cancelDeviceReservation(Long id) {
        var entity = getEntityById(id);
        entity.setBooked(false);
        entity.setBookedOn(null);
        entity.setBookedBy(null);
        entity = deviceRepository.save(entity);
        return deviceMapper.convertToExternalObject(entity);
    }

    @Override
    public DeviceDTO getById(Long id) {
        return deviceMapper.convertToExternalObject(getEntityById(id));
    }

    @Override
    public List<DeviceDTO> getAll() {
        return deviceRepository.findAll()
                .stream()
                .map(deviceMapper::convertToExternalObject)
                .toList();
    }

    private DeviceEntity getEntityById(Long id) {
        return deviceRepository.findById(id)
                .orElseThrow(() -> new DeviceNotFoundException(id));
    }
}
