package com.seryozha.hovhannisyan.testingdevice.service;

import com.seryozha.hovhannisyan.testingdevice.model.BookDeviceRQDTO;
import com.seryozha.hovhannisyan.testingdevice.model.DeviceDTO;

import java.util.List;


/**
 * Strategy interface for reservation of devices.
 *
 * @author Seryozha Hovhannisyan
 */
public interface DeviceService {

    /**
     * Try to reserve a device. Treat as an error if the device can't be found.
     *
     * @param id    the identifier of device to look up, e.g. '5'.
     * @param rqdto a request data transfer object that will pass who booked the device
     * @throws com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException if no corresponding device was found
     */
    DeviceDTO reserveDevice(Long id, BookDeviceRQDTO rqdto);

    /**
     * Try to cancel a device reservation. Treat as an error if the device can't be found.
     *
     * @param id the identifier of device to look up, e.g. '5'.
     * @throws com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException if no corresponding device was found
     */
    DeviceDTO cancelDeviceReservation(Long id);


    /**
     * Try to retrieve a device. Treat as an error if the device can't be found.
     *
     * @param id the identifier of device to look up, e.g. '5'.
     * @throws com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException if no corresponding device was found
     */
    DeviceDTO getById(Long id);

    /**
     * Try to retrieve all devices. Treat as an error if the device can't be found.
     *
     * @throws com.seryozha.hovhannisyan.testingdevice.exception.DeviceNotFoundException if no corresponding device was found
     */
    List<DeviceDTO> getAll();
}
