## Application Database setup, configuration and migration 

The application uses:

PostgreSQL database 12.4 on locale environment.

### Database configuration

The following datasource and jpa is used for database configuration.
```yml
spring:
  # Database
   datasource:
      url: ${JDBC_CONNECTION_URL:jdbc:postgresql://localhost:5432/interview?currentSchema=device-for-testing}
      username: ${JDBC_USERNAME:postgres}
      password: ${JDBC_PSW:changeme}
      type: com.zaxxer.hikari.HikariDataSource
  # JPA properties
  jpa:
    hibernate:
      ddl-auto: none
      show-sql: true
      use_sql_comments: true
    database: postgresql
    database-platform: org.hibernate.dialect.PostgreSQLDialect
```

Although database differences(that depends on environments) we have one configuration for all of them. Using SPEL (Spring Expression Language) allow us to use external configuration(for production and stage environments)  with default values (for locale environment).

### Database migration

For database migration we are using Flyway database migration tool(and flyway plugin with dev profile).

The flyway use versioned sql scripts. The sql scripts are located on [folder](../src/main/resources/db/migration).

All sql scripts filenames has common pattern.

V[version.major version.minor version]__[file description].sql

>For make changes on database
>> create a sql file with name using greater version (can be major or minor version).
