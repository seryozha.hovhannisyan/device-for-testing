# Project structure

## Project folders

| Path                  | Description                           |
| -----------------     | ------------------------------------- |
| `docs/`               | Project documentation folder          |
| `src/main/java`       | Sources                               |
| `src/main/resources`  | Configurations/Resources              |
| `src/test/java`       | Tests                                 |
| `src/test/resources`  | Test Resources                        |

## Service and configuration

| Path                | Description                                    |
| ------------------- | ---------------------------------------------- |
| `.gitignore`        | Git ignore file                                |
| `pom.xm`            | Maven pom file for project                     |


## Project Structure

The project has following structure.

```..  code::
.
|-- com
|   |-- seryozha
|   |   |-- hovhannisyan
|   |   |   | testingdevice
|   |   |   |   |-- api
|   |   |   |   |   |-- ApiResponseEntityExceptionHandler
|   |   |   |   |   |-- DeviceController
|   |   |   |   |-- dao
|   |   |   |   |   |-- entity
|   |   |   |   |   |-- repository
|   |   |   |   |   |   |-- converter
|   |   |   |   |-- exception
|   |   |   |   |-- mapper
|   |   |   |   |-- model
|   |   |   |   |-- service
|   |   |   |   |   |-- impl
|   |   |   |   |   |   |-- DeviceServiceImpl
|   |   |   |   |   |-- DeviceService
```




| Package             | Description                                                                                  |
| ------------------- | -------------------------------------------------------------------------------------------- |
| `api`               | RestControllers, Controller Advice, mappers if needed ex. versioned controllers              |
| `entity`            | external data representation objects, ex: persistent objects                                 |
| `mapper`            | As a mapper tool, we use mapstruct, and Mapper components will be stored in this package     |
| `model`             | All objects which are ready to transfer by API, ex: dto                                      |
| `repository`        | Data access components, such as RestClient,DBClient                                          |
| `rqdto`             | in this package stored all request objects                                                   |
| `service`           | All Business logic                                                                           |

## Temporary objects, ignored in sources

| Path            | Description                                |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `.idea, .idea, *.ipr, *.iml, *.iws, *.ipr, */target, /target, .run`                                                                            | IntelliJ dependencies folders and files      |
| `nb-configuration.xml, /nbproject/private/, /nbbuild/, /dist/, /nbdist/, /.nb-gradle/, build/, !**/src/main/**/build/, !**/src/test/**/build/` | NetBeans dependencies folders and files      |
| `.project, .classpath, .settings/, *.launch, bin/`                                                                                             | Eclipse dependencies folders and files       |
| `.project, .classpath, .settings/, *.launch, bin/`                                                                                             | Visual Studio dependencies folders and files |
| `.DS_Store`                                                                                                                                    | OSX                                          |
| `*.swp, *.swo`                                                                                                                                 | Vim                                          |
| `*.orig, *.rej`                                                                                                                                | patch                                        |
| `.apt_generated,.springBeans, .sts4-cache`                                                                                                     | STS                                          |

## Project UML diagrams

1. For more detail please visit [DeviceController](/src/main/java/com/seryozha/hovhannisyan/testingdevice/api/DeviceController.java)

![DeviceController](./images/DeviceController.png)


2. For more detail please visit [DeviceServiceImpl](/src/main/java/com/seryozha/hovhannisyan/testingdevice/service/impl/DeviceServiceImpl.java)

![DeviceServiceImpl](./images/DeviceServiceImpl.png)

3. For more detail please visit [DeviceMapper](/src/main/java/com/seryozha/hovhannisyan/testingdevice/mapper/DeviceMapper.java)

![DeviceMapper](./images/DeviceMapperImpl.png)

4For more detail please visit [DeviceEntity](/src/main/java/com/seryozha/hovhannisyan/testingdevice/dao/entity/DeviceEntity.java)

![DeviceEntity](./images/DeviceEntity.png)