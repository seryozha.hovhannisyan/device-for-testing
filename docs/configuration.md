# Configuration 

> The configuration is main part of application.
> Configuration allow to setup main data to have independence on any environment.

## Application Configuration

The [application.yml](../src/main/resources/application.yaml) is general configuration. 
The application.yml provide settings for

1. Spring
    - application    - The application name
    - datasource     - The database connection settings
    - jpa            - The hibernate setting 
    - flyway         - The database migration setting
   
> Please follow documentation structure when you will need to make changes on them.

> Please use ${ConfigPropertyName:ConfigPropertyDefaultValue} SPEL specification to allow make external configuration for property.

## Messages Bundle

> The all messages provided in English language (Locale.US) for application.

The application's client messages are flexible and configurable. 
For support new languages we can add new messages_hy.properties.

The client messages are located [messages.properties](../src/main/resources/messages.properties) file.

On messages.properties provided

> Please use provided message's key's structure during adding/modifying new messages.

1. Request Data Transfer Objects validation client messages
   
   The message key has RQDTO-Name.ValidationType.FieldName structure, where
   - RQDTO-Name is Request Data Transfer Object class name, 
   - ValidationType is any javax validation type, 
   - FieldName is Request Data Transfer Object validatable field name

2. Business logic support client messages
   
   This part contains business logic thrown RuntimeException message which are handled on
   [controller advice ApiResponseEntityExceptionHandler java class](../src/main/java/com/seryozha/hovhannisyan/testingdevice/api/ApiResponseEntityExceptionHandler.java) .
   
   The messages key has [exception.message.] prefix.

## Flyway Configuration

> The flyway is database migration tool, that will allow application to have versioned database structure.

The main part of application's database setuped via flyway migration tool.
The flyway related configuration's queries are located on  [directory](../src/main/resources/db/migration/).

For more details please read [Application Database configuration and migration](../docs/database.md) doc.
