## Technology Stack

### Common
Application is going to be written in java.

| Tech               | Version     |
|--------------------|-------------|
| JDK                | 17          |
| SpringBoot         | 3.1.4       | 
| Spring AOP         | 3.1.4       | 
| Spring Data JPA    | 3.1.4       | 
| Spring Validation  | 3.1.4       | 
| Spring Security    | 3.1.4       | 
| Spring Actuator    | 3.1.4       | 
| Lombok             | 1.18.20     | 
| Mapstruct          | 1.4.2.Final | 
| Log4j2             | 3.1.4       | 
| springdoc-openapi  | 2.1.0       | 


### Database related technologies

For DB, we are using DB and flyway.

| Tech          | Local Version   |
| ------------- |-----------------|
| DB            | PostgreSQL 12.4 |
| Flyway        | 7               |


### Plugins

For project, we are using 

- flyway-maven-plugin allow us to migrate database. For use plugin, please follow bash steps.

In the project folder:

```bash
mvn flyway:clean flyway:migrate 
```