# device-for-testing
# Device for testing (back-end)

> This application contains devices for testing specific APIs

## Architecture
The device-for-testing is provided REST API's for 
1. Reserve a device
2. Cancel a device reservation
3. Get device(s) state information


### API Endpoints

For more detail please visit [Swaager](http://localhost:8080/swagger-ui/index.html#/)

![Database production structure](/docs/images/swagger.png)

## Developer docs

- [Project structure](./docs/project-structure.md)
- [Application configuration](./docs/configuration.md)
- [Application Database setup, configuration and migration](./docs/database.md)
- [Technology Stack](./docs/technologies.md)